
//写一个递归方法，输入一个非负整数，返回组成它的数字之和. 例如，输入 1729, 则应该返回
//1+7+2+9，它的和是19
public class Test {
    public static int theSumOfEachBit(int x) {
        if(x < 10) {
            return x;
        }
        return x % 10 + theSumOfEachBit(x / 10);
    }
    public static void main(String[] args) {
        int ret = theSumOfEachBit(1234);
        System.out.println(ret);
    }
}
