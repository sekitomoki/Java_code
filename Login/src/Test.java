import java.util.Scanner;
/**
 * 编写代码模拟三次密码输入的场景.最多能输入三次密码，
 密码正确，提示 “登录成功”,密码错误， 可以重新输 入，最多输入三次。
 三次均错，则提示退出程序
 */

public class Test {
    public static void main(String[] args) {
        String password = "123456";
        int ordinal = 0;

        Scanner scanner = new Scanner(System.in);
        while(ordinal < 3) {
            System.out.print("请输入密码：");
            //键盘录入密码
            String input = scanner.nextLine();
            //判断密码是否正确
            boolean judge = input.equals(password);
            if(judge) {
                System.out.println("登录成功");
                break;
            }
            ordinal++;
            if(ordinal < 3) {
                System.out.println("密码错误，请重新输入。");
            }
        }
        if(ordinal == 3) {
            System.out.println("退出程序");
        }
        scanner.close();
    }
}
