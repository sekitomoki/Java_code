/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-08-31
 * Time: 23:28
 */

class OuterClass {
    public int data1 = 11;
    public static int data2 = 22;
    private int data3 = 33;

    /*//实例内部类
    class InnerClass {
        public int data4 = 44;
        //public static int data5 = 55;//error
        public static final int data5 = 55;
        private int data6 = 66;

        public void test() {
            System.out.println("InnerClass::test() ");
            System.out.println(OuterClass.this.data1);
            System.out.println(OuterClass.this.data2);
            System.out.println(OuterClass.this.data3);
            System.out.println(this.data4);
            System.out.println(this.data5);
            System.out.println(this.data6);
        }
    }*/


    static class InnerClass {
        public int data4 = 44;
        //public static int data5 = 55;//error
        public static final int data5 = 55;
        private int data6 = 66;

        public void test() {
            System.out.println("InnerClass::test() ");
            OuterClass outerClass = new OuterClass();
            //静态的类当中不能直接调用非静态的成员，通过实例化外部类对象，通过对象访问非静态的成员
            System.out.println(outerClass.data1);
            System.out.println(data2);
            System.out.println(outerClass.data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);
        }
    }
    public void test() {
        System.out.println("OuterClass::test() ");
    }
}

public class Test {
    public static void main(String[] args) {
        /*//实例化实例内部类对象要先实例化外部类对象
        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass innerClass = outerClass.new InnerClass();
        innerClass.test();*/

        //实例化 静态内部类对象
        OuterClass.InnerClass innerClass = new OuterClass.InnerClass();
        innerClass.test();
    }
}
