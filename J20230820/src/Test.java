class Animal {
    public String name;
    public int age;
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void eat() {
        System.out.println(this.name + "正在吃饭！");
    }
}
class Dog extends Animal {
    public Dog(String name, int age) {
        super(name, age);
    }
    public void bark() {
        System.out.println(this.name + "汪汪叫");
    }

    @Override
    public void eat() {
        System.out.println(this.name + "正在吃狗粮！");
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

class Cat extends Animal {
    public Cat(String name, int age) {
        super(name, age);
    }
    public void miaow() {
        System.out.println(this.name + "喵喵叫");
    }

    @Override
    public void eat() {
        System.out.println(this.name + "正在吃猫粮！");
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
public class Test {
    public static void eatFunc(Animal animal) {//引用的对象是向上转型（可以是Dog,也可以是Cat）
        animal.eat();//但是调用这一个方法 表现出的行为是不一样的
        //当父类引用的子类对象不一样的时候，调用这个重写的方法 所表现出来的行为是不一样的，这种思想就是多态
    }
    public static void main(String[] args) {
        Dog dog1 = new Dog("圈圈",8);
        Cat cat1 = new Cat("十三月",9);
        System.out.println(dog1);//子类重写了父类的toString方法，Dog{name='圈圈', age=8}
        eatFunc(dog1);
        System.out.println(cat1);
        eatFunc(cat1);
    }
}
