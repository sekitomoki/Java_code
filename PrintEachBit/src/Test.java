
// 按顺序打印一个数字的每一位(例如 1234 打印出 1 2 3 4)
public class Test {
    public static void printEachBit(int x) {
        if(x < 10) {
            System.out.println(x);
            return ;
        }
        printEachBit( x / 10 );
        System.out.println(x % 10);
    }
    public static void main(String[] args) {
        printEachBit(1234);
    }
}
