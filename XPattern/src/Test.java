import java.util.Scanner;


// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Test {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int a = in.nextInt();
            for(int i = 0; i < a; i++) {
                for(int j = 0; j < a; j++) {
                    //i和j相当于横纵坐标，i==j是正斜线的判断条件，(i + j) == a - 1是反斜线的判断条件
                    if(i == j || (i + j) == a - 1) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
}
