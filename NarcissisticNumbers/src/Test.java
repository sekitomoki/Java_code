
//“水仙花数”是指一个三位数，其各位数字的立方和确好等于该数
//本身，如： 153＝1^3＋5^3＋3^3 ，则153是一个“水仙花数”。
public class Test {
    public static void main(String[] args) {
        for (int i = 1; i < 99999; i++) {
            //算出i的位数
            int count = 0;
            int tmp = i;
            while(tmp != 0) {
                tmp = tmp / 10;
                count++;
            }

            int sum = 0;
            tmp = i;
            for (int j = 0; j < count; j++) {
                sum = sum + (int)Math.pow(tmp%10 ,count);//注意有几位数，就乘以几次方
                tmp = tmp / 10;
            }

            if(sum == i) {
                System.out.println(sum);
            }
        }
    }
}
