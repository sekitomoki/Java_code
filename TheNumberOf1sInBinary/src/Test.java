
//写一个方法返回该参数的二进制中 1 的个数
public class Test {
    //这里写了两个方法，表示写了两种解题方法
    public static int numOf1sInBinary1(int x) {
        int tmp = x;
        int count = 0;
        //每次与 x 的最低位按位与 1 ，如果结果是 1，那么最低位就是 1，计数器count++
        //然后 x 无符号向右移1位。接着继续判断最低位是否是 1，如此循环
        while(tmp != 0) {
            if((tmp & 1) == 1) {
                count++;
            }
            tmp = tmp >>> 1;
        }
        return count;
    }
    public static int numOf1sInBinary2(int x) {
        int tmp = x;
        //记录进行按位与的次数
        int count = 0;
        //在每次按位与操作中，都会消去x的最右边的1，
        //因此按位与的次数就等于1的个数。这是一种比较高效的方法。
        while(tmp != 0) {
            tmp = tmp & (tmp-1);
            count++;
        }
        return count;
    }
    public static void main(String[] args) {
        int ret  = numOf1sInBinary1(5);
        System.out.println(ret);
        ret = numOf1sInBinary2(5);
        System.out.println(ret);
    }
}
