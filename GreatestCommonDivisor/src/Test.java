
//求两个整数的最大公约数
public class Test {
    public static void main(String[] args) {
        int num1 = 45;
        int num2 = 63;
        while(num1 % num2 != 0) {
            int num3 = num1 % num2;
            num1 = num2;
            num2 = num3;
        }
        System.out.println(num2);
    }
}
