/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-09-20
 * Time: 23:22
 */
class MyArray<T> {
    private Object[] array = new Object[10];

    public T getArray(int pos) {
        return (T)array[pos];
    }

    public void setArray(int pos,T num) {
        this.array[pos] = num;
    }
}

public class Main {

    public static void main(String[] args) {
        MyArray<Integer> myArray = new MyArray<>();
        myArray.setArray(0,10);
        myArray.setArray(1,100);
        System.out.println(myArray.getArray(0));
        System.out.println(myArray.getArray(1));
    }
}
