
//数一下 1到 100 的所有整数中出现多少个数字9
public class Test {
    public static void main(String[] args) {
        int quantity = 0;
        for (int i = 1; i < 100; i++) {
            if(i % 10 == 9) {
                quantity++;
            }
            if(i / 10 == 9) {
                quantity++;
            }
        }
        System.out.println(quantity);
    }
}
