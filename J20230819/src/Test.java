
//父类Animal
class Animal {
    //父类有两个属性 name 和 age
    public String name;
    public int age;
    //提供带有两个参数的构造方法
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }
    //eat方法
    public void eat() {
        System.out.println(this.name + "正在吃饭！");
    }
}
//Dog类继承了Animal类
class Dog extends Animal {
    public Dog(String name, int age) {
        //帮助初始化了子类从父类继承下来的成员
        super(name, age);
    }
    public void bark() {
        System.out.println(this.name + "汪汪叫");
    }
}

public class Test {
//        public static void main(String[] args) {
//        Dog dog1 = new Dog("圈圈",8);
//        dog1.eat();
//        dog1.bark();
//        System.out.println("===============");
//        Animal animal = new Animal("nb",6);
//        animal.eat();
//        //animal.bark();//error,通过父类引用，只能调用父类自己特有的成员方法 或者 成员变量
//    }
    public static void main(String[] args) {

//        Dog dog1 = new Dog("圈圈",8);
//        //animal这个引用 指向了 dog 这个引用 所指向的对象
//        Animal animal = dog1;//dog1 是 Dog 数据类型的引用，animal 是 Animal 数据类型的引用，
//        // 能够赋值是因为它们的数据类型之间存在父子关系

        //把上面两行代码写成一行
        //animal这个引用 指向了  new Dog("圈圈",8) 这个引用 所指向的对象
        //子类对象Dog给到了父类类型的引用，这就是向上转型
        Animal animal = new Dog("圈圈",8);
        animal.eat();
    }
}
