package lab;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:  Assignment 2
 * User: SEKI TOMOKI
 * Date: 2023-09-07
 * Time: 16:34
 */

class Account {
    private String accountNum;
    private int balance;
    private Account[] accountInformation;

    //调用不带参数的构造方法时，才对账户信息进行实例化
    public Account() {
        accountInformation = new Account[]{
                new Account("123456",1000),
                new Account("999999",2000)
        };
    }

    //调用带两个参数的构造方法时，不对账户信息进行实例化，只实例化账号和余额
    public Account(String accountNum, int balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public Account[] getAccountInformation() {
        return accountInformation;
    }

    public String getAccountNum() {
        return this.accountNum;
    }
    public void getBalance() {
        System.out.println("The balance in your account (" + this.accountNum + ") is " + this.balance);
    }

    public void deposit() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the amount of money you want to deposit from your account: ");
        int num = scanner.nextInt();
        this.balance += num;
        System.out.println("You have been deposited " + num + " to your account. ");
        System.out.println("Now,your balance in your account is: " + this.balance);
    }
    public void withdraw() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the amount of money you want to withdraw from your account: ");
        int num = scanner.nextInt();
        if(num < this.balance) {
            this.balance -= num;
            System.out.println("You have been withdrawn " + num + " in your account. ");
            System.out.println("Now,your balance in your account is: " + this.balance);
        }else {
            System.out.println("Your account (" + this.accountNum + ") does not have enough money " +
                    "to be withdrawn.");
        }
    }

    public int menu() {
        System.out.println("************************");
        System.out.println("    1. get balance      ");
        System.out.println("    2.  deposit         ");
        System.out.println("    3.  withdraw        ");
        System.out.println("    0.   exit           ");
        System.out.println("************************");
        System.out.println("please choose the service you want to (1/2/3/0):");
        Scanner scanner = new Scanner(System.in);
        int choose = scanner.nextInt();
        return choose;
    }
}

public class AccountDemo {

    public static Account login(Account account) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hello,please input your account number first:");
        String inputAccount = scanner.nextLine();

        //遍历已经初始化的对象数组，
        //对象数组中的每个元素都是一个账户对象
        // 把对象数组中的每个元素的账号与用户输入的账号进行比较，如果相同，返回该账户对象在数组中的下标
        int index = -1;
        for (int i = 0; i < account.getAccountInformation().length; i++) {
            Account accountInformation = account.getAccountInformation()[i];
            String accountNum = accountInformation.getAccountNum();
            if(accountNum.equals(inputAccount)) {
                index = i;
                System.out.println("Account number:" + accountNum + " , Welcome to the account system.");
                break;
            }
        }
        if(index != -1) {
            //返回与用户输入相同账号的账户对象
            return account.getAccountInformation()[index];
        }else {
            //说明在初始化的对象数组中，没有该用户的账户
            System.out.println("This account is not registered.");
            return null;
        }

    }

    public static void main(String[] args) {
        Account account = new Account();

        Account userAccount = login(account);

        while(userAccount != null) {
            int choose = userAccount.menu();
            if(choose == 1) {
                userAccount.getBalance();
            }else if(choose == 2) {
                userAccount.deposit();
            }else if(choose == 3){
                userAccount.withdraw();
            }else {
                System.out.println("exit");
                break;
            }
        }

    }
}

