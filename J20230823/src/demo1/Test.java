
package demo1;

//被abstract修饰的类就是抽象类
abstract class Shape{
    //被abstract修饰的方法就是抽象方法
    public abstract void draw();
}

class Circle extends Shape {
    @Override
    public void draw() {

    }
}

class Rectangle extends Shape {
    @Override
    public void draw() {

    }
}
public class Test {
    public static void drawMap(Shape shape) {
        shape.draw();
    }

    public static void main(String[] args) {
        //Shape shape=new Shape();//抽象类不能实例化
        Shape shape1 = new Circle();
        Shape shape2 = new Rectangle();
        drawMap(shape1);
        drawMap(shape2);

    }
}