import book.BookList;
import user.AdministratorUser;
import user.NormalUser;
import user.User;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description: main方法
 */
public class Main {

    public static User login() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入您的姓名:");
        String name = scanner.nextLine();
        System.out.println("请输入您的身份(1/2): 1.管理员用户  2.普通用户 ");
        int choice = scanner.nextInt();

        if(choice == 1) {
            return new AdministratorUser(name);//发生了向上转型
        }else {
            return new NormalUser(name);//发生了向上转型
        }

    }
    public static void main(String[] args) {
        BookList bookList = new BookList();
        User user = login();
        while(true) {
            int choice = user.menu();

            //根据choice 的选择 决定这个类调用的是哪个方法？
            user.doOperation(choice,bookList);
        }
    }
}
