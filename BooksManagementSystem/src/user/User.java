package user;

import book.BookList;
import operation.IOperation;

/**
 * Created with IntelliJ IDEA
 * Description:  用户

 */
public abstract class User {
    protected String name;

    protected IOperation[] iOperations;

    public User(String name) {
        this.name = name;
    }

    public void doOperation(int choice, BookList bookList) {
        this.iOperations[choice].work(bookList);
    }

    public abstract int menu();
}
