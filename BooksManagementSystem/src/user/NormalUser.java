package user;

import operation.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:  普通用户
 */
public class NormalUser extends User{

    public NormalUser(String name) {
        super(name);
        this.iOperations = new IOperation[]{
                new ExitOperation(),
                new FindOperation(),
                new BorrowOperation(),
                new ReturnOperation()};
    }

    @Override
    public int menu() {
        System.out.println("**************************");
        System.out.println("Hell0 " + this.name + ",欢迎来到普通用户菜单！");
        System.out.println("  1. 查找图书");
        System.out.println("  2. 借阅图书");
        System.out.println("  3. 归还图书");
        System.out.println("  0. 退出系统");
        System.out.println("**************************");

        System.out.println("请输入需要执行的服务(1/2/3/0):");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }


}
