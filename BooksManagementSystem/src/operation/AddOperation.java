package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:  新增图书
 */
public class AddOperation implements IOperation{

    @Override
    public void work(BookList bookList) {
        System.out.println("新增图书...");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入新增图书的书名:");
        String name = scanner.nextLine();

        System.out.println("请输入新增图书的作者:");
        String author = scanner.nextLine();

        System.out.println("请输入新增图书的价格:");
        int price = scanner.nextInt();

        String absorbEnter = scanner.nextLine();//吸收回车符, 或者把输入价格放到最后

        System.out.println("请输入新增图书的类别:");
        String category = scanner.nextLine();

        Book book = new Book(name,author,price,category);

        //先检查书架上是否有一样的书
        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize; i++) {
            Book book1 = bookList.getBook(i);
            if(book1.getName().equals(name)) {
                System.out.println("已经有这本书了，无需新增这本图书。");
                return ;
            }
        }

        if(currentSize == bookList.getBooks().length) {//书的数量如果等于数组对象的长度，那么表示书架已经放满书了
            System.out.println("书架上已经放满书啦，不能再新增图书了。");
        }else {
            bookList.setBook(currentSize, book);//新增的书默认放在书架上最后一本书的后面
            bookList.setUsedSize(currentSize + 1);//记得此时书架上面的书已经多了一本，记得书的数量要 +1
            System.out.println("新增图书完成啦！");
        }
    }
}
