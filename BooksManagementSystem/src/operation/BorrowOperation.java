package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description: 借阅图书
 */
public class BorrowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("借阅图书...");

        System.out.println("请输入您要借阅的图书名:");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                book.setBookBorrowed(true);//表示这本书已被借阅
                System.out.println("借阅成功！");
                System.out.println(book);
                return;
            }
        }
        System.out.println("没有找到您要找的书捏。");
    }
}
