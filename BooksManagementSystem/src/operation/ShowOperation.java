package operation;

import book.BookList;

/**
 * Created with IntelliJ IDEA
 * Description:  显示图书
 */
public class ShowOperation implements IOperation{
    @Override
    public void work(BookList bookList){
        System.out.println("显示所有图书！");

        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize ; i++) {
            System.out.println(bookList.getBook(i));
        }
    }
}
