package operation;

import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:  查找图书
 */
public class FindOperation implements IOperation{
    @Override
    public void work(BookList bookList){
        System.out.println("请输入您要查找的书名:");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.println("正在查找图书...");

        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize; i++) {
            if(bookList.getBook(i).getName().equals(name)) {
                System.out.println("该图书找到啦！以下是该图书的信息:");
                System.out.println(bookList.getBook(i));
                return ;
            }
        }
        System.out.println("没有找到该图书捏");
    }
}
