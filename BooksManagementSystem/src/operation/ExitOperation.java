package operation;

import book.BookList;

/**
 * Created with IntelliJ IDEA
 * Description:  退出系统
 */
public class ExitOperation implements IOperation{
    public void work(BookList bookList){
        System.out.println("退出系统");
        System.exit(0);
    }
}
