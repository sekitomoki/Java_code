package operation;

import book.BookList;

/**
 * Created with IntelliJ IDEA
 * Description:

 */
public interface IOperation {
    void work(BookList bookList);
}
