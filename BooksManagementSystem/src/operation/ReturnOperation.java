package operation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description: 归还图书
 */
public class ReturnOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("归还图书");

        System.out.println("请输入您要归还的图书名:");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize; i++) {
            Book book = bookList.getBook(i);
            if(book.getName().equals(name)) {
                book.setBookBorrowed(false);//表示这本书已归还
                System.out.println("归还成功！");
                System.out.println(book);
                return;
            }
        }
        System.out.println("您归还的图书不存在");
    }
}
