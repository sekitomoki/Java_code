package book;

/**
 * Created with IntelliJ IDEA
 * Description: 书
 */
public class Book {
    private String name;
    private String author;
    private double price;
    private String type;
    private boolean isBookBorrowed;

    public Book(String name, String author, double price, String type) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isBookBorrowed() {
        return isBookBorrowed;
    }

    public void setBookBorrowed(boolean bookBorrowed) {
        isBookBorrowed = bookBorrowed;
    }

    @Override
    public String toString() {

        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ((isBookBorrowed) ? " 已被借出" : " 未被借出")   +
                '}';
    }
}
