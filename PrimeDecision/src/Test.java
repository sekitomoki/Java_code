
//给定一个数字，判定一个数字是否是素数
public class Test {
    public static void main(String[] args) {
        int num = 97;
        int flag = 1;
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if(num % i ==0) {
                flag = 0;
                break;
            }
        }

        if(flag == 1) {
            System.out.println("这个数是素数");
        } else {
            System.out.println("这个数不是素数");
        }
    }
}
