import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:  简单的控制台版用户登陆程序
 * User: SEKI TOMOKI
 * Date: 2023-09-17
 * Time: 19:57
 */

//自定义用户名异常类
class UserNameException extends Exception {
    public UserNameException(String message) {
        super(message);
    }
}
//自定义密码异常类
class PasswordException extends Exception {
    public PasswordException(String message) {
        super(message);
    }
}

public class Login {
    public String userName = "admin";
    public String password = "123456";

    public void loginInfo(String paUserName,String paPassword)
            throws UserNameException, PasswordException {//因为这个方法可能会抛出这两个异常，所以要声明异常
        if(!paUserName.equals(this.userName)) {
            throw new UserNameException("用户名错误");
        }
        if(!paPassword.equals(this.password)) {
            throw new PasswordException("密码错误");
        }
        System.out.println("登录成功");
    }
    public static void main(String[] args) {
        Login login = new Login();

        try(Scanner scanner = new Scanner(System.in)) {
            System.out.println("请输入用户名：");
            String userName = scanner.nextLine();
            System.out.println("请输入密码：");
            String passWord = scanner.nextLine();
            login.loginInfo(userName,passWord);
        }catch (UserNameException e) {
            e.printStackTrace();
        }catch (PasswordException e) {
            e.printStackTrace();
        }
    }
}
