
/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2024-06-26
 * Time: 10:06
 */

class StudentImplementsComparable implements Comparable<StudentImplementsComparable> {
    public String name;
    public int age;

    public StudentImplementsComparable(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //使用Comparable的缺点:
    //如果需求改变成要比较学生的名字,需要重新全部修改comparaTo方法
    //一旦写好了这一种比较方式以后只能用这一种比较方式了
    @Override
    public int compareTo(StudentImplementsComparable o) {
        if (this.age > o.age) {
            return 1;
        } else if (this.age == o.age) {
            return 0;
        } else {
            return -1;
        }
    }
}



public class Test {
    public static void main(String[] args) {
        StudentImplementsComparable student1 = new StudentImplementsComparable("xiaoming",18);
        StudentImplementsComparable student2 = new StudentImplementsComparable("guanguan",21);
        System.out.println(student1.compareTo(student2));

        System.out.println("=======================");
        Student stu1 = new Student("xiaoming",18);
        Student stu2 = new Student("guanguan",21);
        AgeComparator Act = new AgeComparator();
        NameComparator Nct = new NameComparator();
        System.out.println(Act.compare(stu1, stu2));
        System.out.println(Nct.compare(stu1, stu2));

    }
}
