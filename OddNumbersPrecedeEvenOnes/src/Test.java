import java.util.Arrays;
//调整数组顺序使得奇数位于偶数之前。调整之后，不关心大小顺序。
//
//如数组：[1,2,3,4,5,6]
//
//调整后可能是：[1, 5, 3, 4, 2, 6]
public class Test {
    public static void func1(int[] arr) {
        int i = 0;
        int j = arr.length-1;
        while(i < j) {
            while(i < j && arr[i] % 2 != 0) {
                i++;
            }
            while(i < j && arr[j] % 2 == 0) {
                j--;
            }
            //要当心i++,j--之后 i < j
            if(i < j) {
                int tmp = arr[j];
                arr[j] = arr[i];
                arr[i] = tmp;
            }
        }
    }
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6};
        func1(arr);
        System.out.println(Arrays.toString(arr));
    }
}
