package demo1;

//子类和父类不存在同名的成员变量时，可以直接访问父类的成员变量
/*class Base {
    public int a;
    public int b;
}

class Derived extends Base {
    public int c;
    public void method() {
        a = 1;
        b = 2;
        c = 3;
    }
}
*/

//子类和父类存在同名的成员变量时,优先访问子类的成员变量
/*
class Base {
    public int a = 9;
    public int b = 99;
}

class Derived extends Base {
    public int a = 88;
    public void method() {
        System.out.println("a:"+a);
        System.out.println("b:"+b);

    }
}
*/

//通过子类对象访问父类的成员方法时，如果子类和父类存在同名的成员方法，优先访问子类的成员方法
class Base {
    public int a = 88;
    public void method() {
        System.out.println("调用了父类中的method");
    }
}
class Derived extends Base {
    public int a= 99;
    public void method() {
        System.out.println("调用了子类中的method");
    }
    public void method2() {
        System.out.println("调用了子类中的method2");
    }
    public void test() {
        method();//调用了子类中的method
        method2();//调用了子类中的method2
        super.method();//调用了父类中的method
        System.out.println("===============");
        System.out.println(a);//99
        System.out.println(this.a);//99
        System.out.println(super.a);//88
    }

}
public class Test {
    public static void main(String[] args) {
//        Derived derived = new Derived();
//        derived.method();//a:88 b:99
        Derived derived = new Derived();
        derived.test();
    }
}