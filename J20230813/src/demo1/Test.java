package demo1;

class Animal {
    public String name;
    public int age;

    public void eat() {
        System.out.println(this.name + "正在吃饭！");
    }
}

class Dog extends Animal{

    public void bark() {
        System.out.println(this.name + "正在汪汪叫！");
    }
}

class Cat extends Animal{

    public void miaow() {
        System.out.println(this.name + "正在喵喵叫！");
    }
}


public class Test {

}

