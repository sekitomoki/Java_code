import java.util.Scanner;
/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-09-03
 * Time: 21:46
 */
public class Main {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        Main main = new Main();
        int number = console.nextInt();
        System.out.println(main.palindromeNumber(number));
    }

    public Boolean palindromeNumber(int number) {

        //write your code here......
        int digit = 5;
        boolean flg = true;
        int reverse = 0;
        int tmp = number;

        //把这个数翻转之后和原来的数比较，相等就是回文数，不相等就不是回文数
        while(number != 0) {
            reverse = reverse * 10 + number % 10;
            number /= 10;
        }

        if(tmp != reverse) {
            flg = false;
        }
        return flg;
    }
}
