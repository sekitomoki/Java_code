package demo;

class Animal {
    public String name;
    public int age;

    public void eat() {
        System.out.println(this.name + "正在吃饭！");
    }


    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("Animal(String name, int age)");
    }


    //如果没有写任何的构造方法时，编译器会默认给出下列的构造方法
    public Animal() {

    }

}

class Dog extends Animal{
    public Dog(String name,int age) {
        //super只能在第一行，且构造方法只能调用一次
        //子类对象构造时，需要先调用基类构造方法，然后执行子类的构造方法
        super(name,age);//调用父类的构造方法 帮助初始化子类从父类继承过来的成员，且并不会生成父类对象
        System.out.println("Dog(String name,int age)");
    }

    public void bark() {
        System.out.println(this.name + "正在汪汪叫！");
    }
}

class Cat extends Animal{
    //右击鼠标选中generate -> constructor -> 选中带有两个参数的构造方法
    public Cat(String name, int age) {
        super(name, age);
        System.out.println("Cat(String name,int age)");
    }

    public void miaow() {
        System.out.println(this.name + "正在喵喵叫！");
    }
}


public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog("圈圈",7);
        dog.bark();
        dog.eat();
        System.out.println("=============================");
        Cat cat = new Cat("静静",6);
        cat.miaow();
        cat.eat();
    }
}
