package demo2;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-08-26
 * Time: 21:18
 */
public class Test {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.powerOn();

        Mouse mouse = new Mouse();
        Keyboard keyboard = new Keyboard();

        computer.useService(mouse);
        System.out.println("====================");
        computer.useService(keyboard);

        computer.powerOff();
    }

}
