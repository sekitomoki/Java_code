package demo2;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-08-26
 * Time: 20:53
 */
public class Mouse implements USB{

    @Override
    public void openDevice() {
        System.out.println("鼠标已连接USB接口");
    }

    public void click() {
        System.out.println("疯狂点只因鼠标......");
    }
    @Override
    public void closeDevice() {
        System.out.println("USB已断开鼠标");
    }
}
