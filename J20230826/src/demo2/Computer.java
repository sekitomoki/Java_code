package demo2;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-08-26
 * Time: 20:52
 */
public class Computer {
    public void powerOn() {
        System.out.println("电脑，启动！！！");
    }
    public void powerOff() {
        System.out.println("电脑关机");
    }

    public void useService(USB usb) {
        usb.openDevice();
        if(usb instanceof Mouse) {
            Mouse mouse = (Mouse)usb;
            mouse.click();
        }else if(usb instanceof Keyboard) {
            Keyboard keyboard = (Keyboard)usb;
            keyboard.input();
        }
        usb.closeDevice();
    }
}
