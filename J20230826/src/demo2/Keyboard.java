package demo2;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-08-26
 * Time: 21:10
 */
public class Keyboard implements USB{
    @Override
    public void openDevice() {
        System.out.println("键盘已连接USB接口");
    }

    public void input() {
        System.out.println("疯狂敲打键盘盘......");
    }

    @Override
    public void closeDevice() {
        System.out.println("USB已断开键盘");
    }
}
