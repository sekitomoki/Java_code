package demo2;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-08-26
 * Time: 20:53
 */
public interface USB {
    void openDevice();
    void closeDevice();
}
