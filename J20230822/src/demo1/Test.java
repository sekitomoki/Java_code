package demo1;
//需求：画一个"circle", "rectangle", "circle", "rectangle", "triangle"的图像

class Shape{
    public void draw() {
        System.out.println("画图");
    }
}

class Triangle extends Shape {
    @Override
    public void draw() {
        System.out.println("三角形");
    }
}

class Rectangle extends Shape{
    @Override
    public void draw() {
        System.out.println("长方形");
    }
}

class Circle extends Shape{
    @Override
    public void draw() {
        System.out.println("圆形");
    }
}

class Flower extends Shape{
    @Override
    public void draw() {
        System.out.println("花");
    }
}
public class Test {
    public void drawMap(Shape shape) {
        shape.draw();
    }
    //使用多态的方法
    public static void main(String[] args) {
        //如果想添加图像，只需添加一个对象即可，用多态的方法改动成本低
        Shape[] shapes = {new Circle(),new Rectangle(),new Circle(),new Rectangle(),new Triangle()};
        for (Shape shape : shapes) {
            shape.draw();
        }
    }

    //没有使用多态的方法
    public static void main2(String[] args) {
        Triangle triangle = new Triangle();
        Rectangle rectangle = new Rectangle();
        Circle circle = new Circle();
        String[] image = {"circle", "rectangle", "circle", "rectangle", "triangle"};
        for (String x: image) {
            if(x.equals("circle")) {
                circle.draw();
            }else if(x.equals("rectangle")) {
                rectangle.draw();
            }else {
                triangle.draw();
            }
        }
    }

}
