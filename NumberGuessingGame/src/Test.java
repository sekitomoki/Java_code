import java.util.Random;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Random random = new Random(); // 默认随机种子是系统时间
        int toGuess = random.nextInt(100);

        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.println("请输入数字：(1~100)");
            int input = sc.nextInt();
            if (toGuess == input) {
                System.out.println("猜对了");
                break;
            } else if (toGuess > input) {
                System.out.println("猜小了");
            } else {
                System.out.println("猜大了");
            }
        }
        sc.close();
    }
}
