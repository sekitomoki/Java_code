
//获取一个数二进制序列中所有的偶数位和奇数位，分别输出二进制序列
public class Test {
    public static void main(String[] args) {
        int num = 7;
        System.out.println("所有偶数位是：");
        for (int i = 31; i >=0 ; i-=2) {
            System.out.print(( (num>>>i) & 1 ) + " ");
        }
        System.out.println();
        for (int i = 30; i >=0 ; i-=2) {
            System.out.print(( (num>>>i) & 1 ) + " ");
        }
    }
}
