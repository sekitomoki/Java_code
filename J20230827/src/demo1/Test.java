package demo1;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: SEKI TOMOKI
 * Date: 2023-08-27
 * Time: 11:33
 */


class Animal {
    public String name;
    public int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

interface IRun {
    void run();
}

interface ISwim {
    void swim();
}

interface IFly {
    void fly();
}

class Dog extends Animal implements IRun {

    public Dog(String name,int age) {
        super(name,age);
    }

    @Override
    public void run() {
        System.out.println(name + "用4条腿在奔跑......");
    }
}

class Frog extends Animal implements IRun,ISwim {

    public Frog(String name, int age) {
        super(name, age);
    }

    @Override
    public void run() {
        System.out.println(name + "用两条后腿跳着跑......");
    }

    @Override
    public void swim() {
        System.out.println(name + "展示标准的蛙泳......");
    }
}

class Duck extends Animal implements IRun,ISwim,IFly {

    public Duck(String name, int age) {
        super(name, age);
    }

    @Override
    public void run() {
        System.out.println(name + "用两条鸭腿跑......");
    }

    @Override
    public void swim() {
        System.out.println(name + "用两条鸭腿在水面上游.....");
    }

    @Override
    public void fly() {
        System.out.println(name + "用翅膀飞......");
    }
}

class Robot implements IRun {

    @Override
    public void run() {
        System.out.println("机器人在奔跑......");
    }
}

public class Test {

    public static void running(IRun iRun) {
        iRun.run();
    }
    public static void swimming(ISwim iSwim) {
        iSwim.swim();
    }
    public static void flying(IFly iFly) {
        iFly.fly();
    }
    public static void main(String[] args) {
        //只要 该对象实现了IRun的接口，就能调用 该对象
        running(new Dog("圈圈",8));
        running(new Frog("青蛙王子",3));
        running(new Duck("唐老鸭",6));
        running(new Robot());
        System.out.println("==================");
        swimming(new Frog("青蛙王子",3));
        swimming(new Duck("唐老鸭",6));
        System.out.println("==================");
        flying(new Duck("唐老鸭",6));
    }
}
