package demo2;

import demo1.Test2;

//不同包中的子类可以访问以protected修饰的成员
public class Test extends Test2 {

    public void func() {
        System.out.println(super.a);
    }
    public static void main(String[] args) {
        Test test = new Test();
        test.func();//199
    }
}
