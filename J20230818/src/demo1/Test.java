package demo1;

class Animal {
    public String name;
    public int age;
    //静态代码块
    static {
        System.out.println("demo1.Animal::static{静态代码块}");
    }
    //实例代码块
    {
        System.out.println("demo1.Animal::{实例代码块}");
    }
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("demo1.Animal(String name, int age)");
    }
}

class Dog extends Animal{
    //静态代码块
    static {
        System.out.println("demo1.Dog::static{静态代码块}");
    }
    //实例代码块
    {
        System.out.println("demo1.Dog::{实例代码块}");
    }
    public Dog(String name,int age) {
        super(name,age);
        System.out.println("demo1.Dog(String name,int age)");
    }
}

public class Test {
    public static void main(String[] args) {
        Dog dog1 = new Dog("圈圈",7);
        System.out.println("================");
        Dog dog2 = new Dog("圈圈",8);

    }
}
