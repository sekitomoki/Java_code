import java.util.Scanner;

public class Test {
    public static void hanoi(int n, char A, char B, char C) {
        //如果只有一个盘子，直接从 A 柱挪到 C 柱
        if(n == 1) {
            System.out.println(A + "-->" + C);
        } else {
            //第一步：把上面的 n-1 个盘子从 A 柱挪到 B柱
            hanoi(n-1, A, C, B);
            //第二步；把最底下的盘子从 A 柱挪到 C柱
            System.out.println(A + "-->" + C);
            //第三步：把 n-1 个盘子从 B 柱挪到 C 柱
            hanoi(n-1, B, A, C);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //输入盘子的个数
        int input = sc.nextInt();
        hanoi(input, 'A', 'B', 'C');
        sc.close();
    }
}
